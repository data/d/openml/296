# OpenML dataset: Ailerons

https://www.openml.org/d/296

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Luis Torgo","Rui Camacho  
**Source**: Unknown - 2014-08-18  
**Please cite**:   

This data set addresses a control problem, namely flying a F16 aircraft. The attributes describe the status of the aeroplane, while the goal is to predict the control action on the ailerons of the aircraft.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/296) of an [OpenML dataset](https://www.openml.org/d/296). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/296/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/296/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/296/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

